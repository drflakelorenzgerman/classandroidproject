package ir.ali.classandroidproject.api.model

data class PhotoModel(
    var albumId : Int,
    var id : Int,
    var title : String,
    var url : String,
    var thumbnailUrl : String,
    var a : Any
)


