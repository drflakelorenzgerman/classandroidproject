package ir.ali.classandroidproject.api.model

import ir.ali.classandroidproject.api.ApiServices
import ir.ali.classandroidproject.api.tool.Const.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    private var retrofit : Retrofit ?= null

    fun getInstance() : ApiServices {
        if(retrofit == null){
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit!!.create(ApiServices::class.java)
    }
}