package ir.ali.classandroidproject.api

import ir.ali.classandroidproject.api.model.PhotoModel
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {
    @GET("/photos") fun photo() : Call<List<PhotoModel>>

//    @GET("blawblaw") fun blaw(@Query("id") iddd : Int)
//
//    @GET("blawblaw/{id}") fun blaw(@Path("id") iddd : Int)

//    @POST("blaw")
//    @FormUrlEncoded
//    fun blaw(
//        @Field("id") id : Int,
//        @Field("name") name : String
//        ) : Call<List<PhotoModel>>

//
//    @POST("blaw")
//    fun blaw(
//        @Body obj : MyObj
//    ) : Call<List<PhotoModel>>

}


