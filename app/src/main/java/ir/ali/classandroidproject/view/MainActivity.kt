package ir.ali.classandroidproject.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import ir.ali.classandroidproject.R
import ir.ali.classandroidproject.api.model.PhotoModel
import ir.ali.classandroidproject.api.model.RetrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        RetrofitInstance.getInstance().photo().enqueue(object : Callback<List<PhotoModel>>{

            override fun onFailure(call: Call<List<PhotoModel>>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<List<PhotoModel>>,
                response: Response<List<PhotoModel>>
            ) {
                if(response.body() != null){
                    findViewById<TextView>(R.id.txt).text = response.body()!![0].title
                }
            }

        })

    }
}